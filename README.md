# GitLab CI Yarn Audit Test

Simple project to test different CI configurations to ensure a `yarn audit` is executed but still passes unless a severity threshold is exceeded making use of the [IBM audit-ci](https://github.com/IBM/audit-ci) package.
